import { NodeBlock, assert } from "@tripetto/runner";
import "./condition";

export interface ISignature {
    /** Size of the signature panel. */
    readonly size?: "sm" | "md" | "lg";

    /** Color of the signature. */
    readonly color?: string;
}

export interface ISignatureService {
    /** Fetch a signature from the store. */
    readonly get: (file: string) => Promise<Blob>;

    /** Put a signature in the store. */
    readonly put: (
        file: File,
        onProgress?: (percentage: number) => void
    ) => Promise<string>;

    /** Delete a signature from the store. */
    readonly delete: (file: string) => Promise<void>;
}

export abstract class Signature extends NodeBlock<ISignature> {
    private cache: string | undefined;
    readonly signatureSlot = assert(this.valueOf<string>("signature"));
    readonly required = this.signatureSlot.slot.required || false;

    get isSigning() {
        return this.signatureSlot.isAwaiting;
    }

    get size() {
        return this.props.size || "md";
    }

    get color() {
        return this.props.color || undefined;
    }

    private convertToBase64(
        blob: Blob,
        done: (data: string) => void,
        progress?: (percentage: number) => void
    ): void {
        const reader = new FileReader();

        if (progress) {
            reader.onprogress = (event) =>
                progress((event.loaded / event.total) * 100);
        }

        reader.onload = () => {
            done(reader.result as string);
        };

        reader.readAsDataURL(blob);
    }

    upload(
        files: FileList,
        service?: ISignatureService,
        onProgress?: (percent: number) => void
    ): Promise<void> {
        this.cache = undefined;

        this.signatureSlot.clear();

        return new Promise(
            (resolve: () => void, reject: (error: string) => void) => {
                if (files.length !== 1) {
                    return reject("");
                }

                const file = files[0];

                this.signatureSlot.await();

                if (service) {
                    service
                        .put(file, onProgress)
                        .then((id) => {
                            this.signatureSlot.set(file.name, id);

                            this.convertToBase64(file, (data) => {
                                this.cache = data;

                                resolve();
                            });
                        })
                        .catch((error) => {
                            this.signatureSlot.clear();

                            reject(error);
                        });
                } else {
                    this.convertToBase64(
                        file,
                        (data) => {
                            this.signatureSlot.set(file.name, data);

                            resolve();
                        },
                        onProgress
                    );
                }
            }
        );
    }

    download(service?: ISignatureService): Promise<string> {
        return new Promise(
            (resolve: (data: string) => void, reject: () => void) => {
                if (this.cache) {
                    resolve(this.cache);
                } else if (this.signatureSlot.reference) {
                    if (service) {
                        service
                            .get(this.signatureSlot.reference)
                            .then((blob) =>
                                this.convertToBase64(blob, (data) =>
                                    resolve(data)
                                )
                            )
                            .catch(() => {
                                this.signatureSlot.clear();

                                reject();
                            });
                    } else {
                        resolve(this.signatureSlot.reference);
                    }
                } else {
                    reject();
                }
            }
        );
    }

    delete(service?: ISignatureService): Promise<void> {
        return new Promise((resolve: () => void, reject: () => void) => {
            if (this.signatureSlot.reference && service) {
                this.signatureSlot.await();

                service
                    .delete(this.signatureSlot.reference)
                    .then(() => {
                        this.cache = undefined;
                        this.signatureSlot.clear();

                        resolve();
                    })
                    .catch(() => {
                        this.signatureSlot.cancelAwait();

                        reject();
                    });
            } else {
                this.cache = undefined;
                this.signatureSlot.clear();

                resolve();
            }
        });
    }
}
