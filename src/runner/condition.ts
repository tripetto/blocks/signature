/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class SignatureCondition extends ConditionBlock<{
    readonly isSigned: boolean;
}> {
    @condition
    isSigned(): boolean {
        const signatureSlot = this.valueOf<string>();

        return (
            (signatureSlot && signatureSlot.value ? true : false) ===
            this.props.isSigned
        );
    }
}
