/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    affects,
    definition,
    pgettext,
    tripetto,
} from "@tripetto/builder";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_SIGNED from "../../assets/icon-signed.svg";
import ICON_UNSIGNED from "../../assets/icon-unsigned.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:signature", "Signed state");
    },
})
export class SignatureCondition extends ConditionBlock {
    @definition
    @affects("#name")
    readonly isSigned: boolean = true;

    get icon() {
        return this.isSigned ? ICON_SIGNED : ICON_UNSIGNED;
    }

    get name() {
        return this.isSigned
            ? pgettext("block:signature", "Signed")
            : pgettext("block:signature", "Not signed");
    }

    get title() {
        return this.node?.label;
    }
}
