/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    NodeBlock,
    Forms,
    Slots,
    conditions,
    definition,
    each,
    editor,
    isString,
    pgettext,
    slots,
    tripetto,
} from "@tripetto/builder";
import { SignatureCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_SIGNED from "../../assets/icon-signed.svg";
import ICON_UNSIGNED from "../../assets/icon-unsigned.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:signature", "Signature");
    },
})
export class Signature extends NodeBlock {
    public signatureSlot!: Slots.String;

    @definition("string", "optional")
    size?: "sm" | "md" | "lg";

    @definition("string", "optional")
    color?: string;

    @slots
    defineSlots(): void {
        this.signatureSlot = this.slots.static({
            type: Slots.String,
            reference: "signature",
            label: pgettext("block:signature", "Signature"),
            protected: true,
            exchange: ["required", "alias"],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        this.editor.groups.settings();

        this.editor.option({
            name: pgettext("block:signature", "Color"),
            form: {
                title: pgettext("block:signature", "Color"),
                controls: [
                    new Forms.ColorPicker(
                        Forms.ColorPicker.bind(
                            this,
                            "color",
                            undefined,
                            "darkblue"
                        )
                    )
                        .supportAlpha(false)
                        .placeholder(pgettext("block:signature", "Ink color"))
                        .swatches(false, true),
                    new Forms.Static(
                        pgettext(
                            "block:signature",
                            "Here, you can specify the color of the signature ink when signing. The signature will always be saved using black ink in the dataset."
                        )
                    ),
                ],
            },
            activated: isString(this.color),
        });

        this.editor.option({
            name: pgettext("block:signature", "Size"),
            form: {
                title: pgettext("block:signature", "Size"),
                controls: [
                    new Forms.Radiobutton<"sm" | "md" | "lg">(
                        [
                            {
                                label: pgettext("block:signature", "Small"),
                                value: "sm",
                            },
                            {
                                label: pgettext("block:signature", "Medium"),
                                value: "md",
                            },
                            {
                                label: pgettext("block:signature", "Large"),
                                value: "lg",
                            },
                        ],
                        Forms.Radiobutton.bind(this, "size", undefined, "md")
                    ),
                ],
            },
            activated: isString(this.size),
        });

        this.editor.groups.options();
        this.editor.required(this.signatureSlot);
        this.editor.visibility();
        this.editor.alias(this.signatureSlot);
    }

    @conditions
    defineConditions(): void {
        each(
            [
                {
                    label: pgettext("block:signature", "Signed"),
                    icon: ICON_SIGNED,
                    isSigned: true,
                },
                {
                    label: pgettext("block:signature", "Not signed"),
                    icon: ICON_UNSIGNED,
                    isSigned: false,
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: SignatureCondition,
                    label: condition.label,
                    icon: condition.icon,
                    burst: true,
                    props: {
                        slot: this.signatureSlot,
                        isSigned: condition.isSigned,
                    },
                });
            }
        );
    }
}
